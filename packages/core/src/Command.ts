import type { ICommand, ISnapshot, IModel, CommitTransaction } from './types';


export abstract class Command<M extends IModel, S extends ISnapshot<M>> implements ICommand<M, S> {
   constructor(public name:string) {}

   abstract description:string;

   abstract commit(snapshot:S, options:CommitTransaction):Promise<boolean>;

   /**
    * This method should be overridden in the case where a reset should revert
    * changes performed by the commit. By default, the method will resolve
    * successfully.
    * 
    * Returning a falsy value indicates that the reset failed and no further
    * command should be reset because the state is broken.
    * 
    * @returns a status whether the command was reset successfully.
    */
   reset(/*snapshot:ISnapshot<IModel>, options:CommitTransaction*/):Promise<boolean> {
      return Promise.resolve(true);
   }

}