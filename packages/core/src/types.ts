
export interface IModel {}


export interface ISnapshot<M extends IModel> {
   readonly state:M
}


export type CommitTag = string

export type CommitTransaction = {
   tag:CommitTag
   timestamp:Date
}

interface CommandExecutionMethod<M extends IModel, S extends ISnapshot<M>> {
   (snapshot:S, options:CommitTransaction):Promise<any>
}
export interface ICommand<M extends IModel, S extends ISnapshot<M>> {
   /**
    * Unique name identifying this command
    */
   readonly name:string
   /**
    * Human readable description for this command, used
    * to log commands
    */
   readonly description:string

   commit:CommandExecutionMethod<M, S>
   reset:CommandExecutionMethod<M, S>
}



interface StateIteratorMethod {
   ():Iterator<CommitTag>
}
interface StateGetMethod {
   /**
    * Return the state given a specified offset. The initial
    * state has an offset of 0. A positive offset starts
    * from the initial state. A negative offset starts from
    * the head state.
    * 
    * An invalid offset, for example falling outside of the
    * initial or head states, will return null
    */
   (offset:number):CommitTag|null
}
export interface IStateManager {
   initial:CommitTag
   current:CommitTag
   head:CommitTag

   iterator:StateIteratorMethod
   get:StateGetMethod
}



interface StoreCommitMethod<M extends IModel, S extends ISnapshot<M>> {
   (command:ICommand<M, S>):Promise<boolean>
}
interface StoreResetMethod {
   /**
    * Reset the state to the specific commit tag. If the commit hash
    * is invalid, the preomise will be rejected with an error.
    */
   (tag:CommitTag):Promise<boolean>
}
export interface IStore<M extends IModel, S extends ISnapshot<M>> {
   readonly model:M
   readonly state:IStateManager

   commit:StoreCommitMethod<M, S>
   reset:StoreResetMethod
}