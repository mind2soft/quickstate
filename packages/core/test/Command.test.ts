import { Command, IModel, ISnapshot } from '../src';


describe('Testing Command', () => {
  
  it('should create instance', () => {
    class DummyModel implements IModel {}
    class DummySnapshot implements ISnapshot<DummyModel> {
      get state() { return { foo:true }; }
    }

    class DummyCommand extends Command<DummyModel, DummySnapshot> {
      constructor() { super("Test Command"); }
      get description(): string { return "Dummy command"; }

      commit(/*snapshot: DummySnapshot, options: CommitTransaction*/): Promise<any> {
        

        return Promise.resolve({ foo: false });
      }

    }

    const command = new DummyCommand();

    expect(command.name).toBe("Test Command");
  });

});
