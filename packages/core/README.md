# QuickState Core

Bidirectional state management for the win!



## Use case

```ts
import { Store, StateManager, Command, IModel, ISnapshot } from '@quickstate/core';

// 1. Declare model
class NumberModel implements Model {
  constructor(public value:number) { }
}

// 2. Declare snapshot that can serialize a Model
class NumberSnapshot implements Snapshot<NumberModel> {
  private state:NumberModel

  constructor(model:NumberModel) {
    this.state = Object.freeze({ value: model.value });
  }
}

// 3. Declare a command that will be applied on the model
class IncrementCommand extends Command<NumberModel, NumberSnapshot> {
  constructor(private increment:number) { }

  // returns a delta of the modified model, only modified fields
  // should be returned
  commit(snapshot:NumberSnapshot) {
    return { value: snapshot.state.value + increment };
  }

  // returns a delta of the reverted model, only modified fields
  // should be returned
  reset(snapshot:NumberSnapshot) {
    return { value: snapshot.state.value - increment };
  }
}

const INIT_VALUE = 14;

// 4. Create a model store
const store = new Store(new NumberModel(INIT_VALUE));

// 5. Create a command to mutate the model
const command1 = new IncrementCommand(11);
const command2 = new IncrementCommand(17);

console.log("Current state is:", store.state.current);     // ex: "a1b2c3"
console.log("Current store value is:", store.model);       // -> { value: 14 }
console.log("Applying change, state is:", await store.commit(command1) ? "Success" : "Error");
console.log("Applying change, state is:", await store.commit(command2) ? "Success" : "Error");
console.log("Current state is:", store.state.current)      // ex: "f3g4h5"
console.log("Current store value is:", store.model);       // -> { value: 42 }
console.log("Reseting to initial:", await store.reset(store.state.initial) ? "Success" : "Error");
console.log("Current state is:", store.state.current)      // ex: "a1b2c3"
console.log("Current store value is:", store.model);       // -> { value: 14 }
```



## Licence

MIT